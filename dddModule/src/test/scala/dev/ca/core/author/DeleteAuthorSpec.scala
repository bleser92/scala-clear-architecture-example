package dev.ca.core.author

import cats.effect.IO
import cats.effect.testing.scalatest.AsyncIOSpec
import dev.ca.core.author.ports.AuthorRepository
import dev.ca.core.author.usecases.{CreateAuthor, DeleteAuthor}
import org.mockito.cats.{IdiomaticMockitoCats, MockitoCats}
import org.mockito.scalatest.{AsyncIdiomaticMockito, AsyncMockitoSugar}
import org.scalatest.Assertion
import org.scalatest.flatspec.AsyncFlatSpec
import org.scalatest.matchers.should.Matchers

class DeleteAuthorSpec
  extends AsyncFlatSpec
    with AsyncIOSpec
    with Matchers
    with IdiomaticMockitoCats
    with AsyncIdiomaticMockito {
  type F[+T] = IO[T]

  private def withDeleteAuthor(testCode: DeleteAuthor[F] => F[Assertion]): F[Assertion] = {
    val mockAuthorRepo = mock[AuthorRepository[F]]
    val useDeleteAuthor = new DeleteAuthor[F](mockAuthorRepo)

    mockAuthorRepo.deleteById(*) returns IO.unit

    testCode(useDeleteAuthor)
  }

  "A DeleteAuthor" should "successfully delete" in withDeleteAuthor { useDeleteAuthor =>
    useDeleteAuthor.execute(1)
      .assertNoException
  }

}
