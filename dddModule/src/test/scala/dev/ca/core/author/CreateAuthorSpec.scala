package dev.ca.core.author

import cats.effect.IO
import cats.effect.testing.scalatest.AsyncIOSpec
import dev.ca.common.exception.ValidationException
import org.scalatest.flatspec.{AnyFlatSpec, AsyncFlatSpec}
import dev.ca.core.author.dto.CreateAuthorDTO
import dev.ca.core.author.entities.Author
import dev.ca.core.author.ports.AuthorRepository
import dev.ca.core.author.usecases.CreateAuthor
import org.mockito.cats.{IdiomaticMockitoCats, MockitoCats}
import org.mockito.scalatest.{AsyncIdiomaticMockito, AsyncMockitoSugar, IdiomaticMockito, MockitoSugar}
import org.scalatest.{Assertion, nocolor}
import org.scalatest.matchers.should.Matchers

class CreateAuthorSpec
  extends AsyncFlatSpec
    with AsyncIOSpec
    with Matchers
    with IdiomaticMockitoCats
    with AsyncIdiomaticMockito {

  type F[+T] = IO[T]

  private def withCreateAuthor(name: String)(testCode: (CreateAuthorDTO, AuthorRepository[F], CreateAuthor[F]) => F[Assertion]) = {
    val mockAuthorRepo = mock[AuthorRepository[F]]
    val useCreateAuthor = new CreateAuthor[F](mockAuthorRepo)
    val dto = CreateAuthorDTO(name)

    testCode(dto, mockAuthorRepo, useCreateAuthor)
  }


  "A CreateAuthor" should "succeed create" in
    withCreateAuthor("author") { (dto, mockRepo, useCreateAuthor) =>

      mockRepo.create(*) returnsF Author(1, dto.name)

      useCreateAuthor.execute(dto)
        .asserting(_ shouldBe Author(1, dto.name))
        .asserting(_ => mockRepo.create(*) was called)
    }

  it should "produce ValidationException when name is empty" in
    withCreateAuthor("") { (dto, mockRepo, useCreateAuthor) =>

      useCreateAuthor.execute(dto)
        .assertThrows[ValidationException]
        .asserting(_ => mockRepo.create(*) wasNever called)
    }

}
