package dev.ca.core.author

import cats.effect.IO
import cats.effect.testing.scalatest.AsyncIOSpec
import dev.ca.core.author.entities.Author
import dev.ca.core.author.ports.AuthorRepository
import dev.ca.core.author.usecases.{DeleteAuthor, GetListAuthor}
import org.mockito.cats.IdiomaticMockitoCats
import org.mockito.scalatest.AsyncIdiomaticMockito
import org.scalatest.Assertion
import org.scalatest.flatspec.AsyncFlatSpec
import org.scalatest.matchers.should.Matchers

class GetListAuthorSpec
  extends AsyncFlatSpec
    with AsyncIOSpec
    with Matchers
    with IdiomaticMockitoCats
    with AsyncIdiomaticMockito {

  type F[+T] = IO[T]

  private def withGetListAuthor(testCode: GetListAuthor[F] => F[Assertion]): F[Assertion] = {
    val mockAuthorRepo = mock[AuthorRepository[F]]
    val useGetListAuthor = new GetListAuthor[F](mockAuthorRepo)

    mockAuthorRepo.getAll() returnsF List.range(1, 5).map(i => Author(i + 1, s"author-$i"))

    testCode(useGetListAuthor)
  }

  "A GetListAuthor" should "return no empty list" in withGetListAuthor { useGetListAuthor =>
    useGetListAuthor.execute()
      .asserting(_ should not be empty)
  }

}
