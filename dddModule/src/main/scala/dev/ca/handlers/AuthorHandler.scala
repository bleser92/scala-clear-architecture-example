package dev.ca.handlers

import cats.effect.Async
import org.http4s.{Request, Response}
import org.http4s.dsl.Http4sDsl
import org.http4s.circe.CirceEntityCodec._
import cats.syntax.all._
import dev.ca.core.author.dto.CreateAuthorDTO
import dev.ca.core.author.ports.AuthorRepository
import dev.ca.core.author.usecases.{CreateAuthor, DeleteAuthor, GetListAuthor}

class AuthorHandler[F[_] : Async] private (
  private val useCreateAuthor: CreateAuthor[F],
  private val useDeleteAuthor: DeleteAuthor[F],
  private val useGetListAuthor: GetListAuthor[F]
) {

  private val dsl = Http4sDsl[F]


  import HandlerCodec._
  import dsl._


  def createAuthor(req: Request[F]): F[Response[F]] = {
    for {
      dto <- req.as[CreateAuthorDTO]
      author <- useCreateAuthor.execute(dto)
      result = HandlerResult.of(CreateAuthorResponse(author))
      res <- Created(result)
    } yield res
  }

  def deleteAuthor(authorId: Long): F[Response[F]] = {
    useDeleteAuthor.execute(authorId)
      .flatMap(_ => NoContent())
  }

  def getListAuthor(): F[Response[F]] = {
    useGetListAuthor.execute()
      .map(GetListAuthorResponse)
      .map(HandlerResult.of)
      .flatMap(Ok(_))
  }

}

object AuthorHandler {
  def apply[F[_] : Async](authorRepo: AuthorRepository[F]): AuthorHandler[F] = {
    val useCreateAuthor = new CreateAuthor(authorRepo)
    val useDeleteAuthor = new DeleteAuthor(authorRepo)
    val useGetListAuthor = new GetListAuthor(authorRepo)

    new AuthorHandler(useCreateAuthor, useDeleteAuthor, useGetListAuthor)
  }
}
