package dev.ca.handlers

import cats.effect.Async
import cats.syntax.all._
import dev.ca.core.book.dto.CreateBookDTO
import dev.ca.core.book.ports.BookRepository
import dev.ca.core.book.usecases.{CreateBook, DeleteBook, GetListBook}
import org.http4s.{Request, Response}
import org.http4s.circe.CirceEntityCodec._
import org.http4s.dsl.Http4sDsl


// или можно назвать его BookHandler
class BookHandler[F[_] : Async] private(
  private val useCreateBook: CreateBook[F],
  private val useDeleteBook: DeleteBook[F],
  private val useGetListBook: GetListBook[F]
) {
  private val dsl = Http4sDsl[F]


  import HandlerCodec._
  import dsl._


  def createBook(req: Request[F]): F[Response[F]] = {
    for {
      dto <- req.as[CreateBookDTO]
      book <- useCreateBook(dto)
      result = HandlerResult.of(CreateBookResponse(book))
      res <- Created(result)
    } yield res
  }

  def deleteBook(bookId: Long): F[Response[F]] = {
    useDeleteBook.execute(bookId)
      .flatMap(_ => NoContent())
  }

  def getListBook(): F[Response[F]] = {
    useGetListBook.execute()
      .map(GetListBookResponse)
      .map(HandlerResult.of)
      .flatMap(Ok(_))
  }



}

object BookHandler {
  def apply[F[_] : Async](bookRepo: BookRepository[F]): BookHandler[F] = {
    val useCreateBook = new CreateBook(bookRepo)
    val useDeleteBook = new DeleteBook(bookRepo)
    val useGetListBook = new GetListBook(bookRepo)

    new BookHandler[F](useCreateBook, useDeleteBook, useGetListBook)
  }
}
