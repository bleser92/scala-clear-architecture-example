package dev.ca.handlers

import cats.implicits.catsSyntaxOptionId

sealed abstract class HandlerResult {
  def data: Option[HandlerResponse] = None
  def error: Option[Throwable] = None
}

case class DataHandlerResult(override val  data: Option[HandlerResponse]) extends HandlerResult
case class ErrorHandlerResult(override val error: Option[Throwable]) extends HandlerResult


object HandlerResult {
  def of(ex: Throwable): HandlerResult = {
    ErrorHandlerResult(ex.some)
  }
  def of(resp: HandlerResponse): HandlerResult = {
    DataHandlerResult(resp.some)
  }
}
