package dev.ca.handlers

import dev.ca.core.author.entities.Author
import dev.ca.core.book.entities.Book
import dev.ca.core.shop.entities.Shop


//Если тут будет слишком много классов будет уже не удобно
// Наверно стоит закинуть всё это в отдельный подпакет
sealed abstract class HandlerResponse


case class CreateBookResponse private(id: Long, name: String) extends HandlerResponse
object CreateBookResponse {
  def apply(book: Book): CreateBookResponse = CreateBookResponse(book.id, book.name)
}

case class GetListBookResponse(books: List[Book]) extends HandlerResponse

case class CreateAuthorResponse(id: Long, name: String) extends HandlerResponse
object CreateAuthorResponse {
  def apply(author: Author): CreateAuthorResponse = CreateAuthorResponse(author.id, author.name)
}

case class GetListAuthorResponse(authors: List[Author]) extends HandlerResponse

case class CreateShopResponse(id: Long, name: String) extends HandlerResponse
object CreateShopResponse {
  def apply(shop: Shop): CreateShopResponse = CreateShopResponse(shop.id, shop.name)
}

case class GetListShopResponse(shops: List[Shop]) extends HandlerResponse





