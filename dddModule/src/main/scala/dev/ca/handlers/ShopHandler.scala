package dev.ca.handlers

import cats.effect.Async
import org.http4s.{Request, Response}
import org.http4s.dsl.Http4sDsl
import org.http4s.circe.CirceEntityCodec._
import cats.syntax.all._
import dev.ca.core.shop.dto.CreateShopDTO
import dev.ca.core.shop.ports.ShopRepository
import dev.ca.core.shop.usecases.{CreateShop, DeleteShop, GetListShop}

class ShopHandler[F[_] : Async] private (
  private val useCreateShop: CreateShop[F],
  private val useDeleteShop: DeleteShop[F],
  private val useGetListShop: GetListShop[F]
) {

  private val dsl = Http4sDsl[F]


  import HandlerCodec._
  import dsl._

  def createShop(req: Request[F]): F[Response[F]] = {
    for {
      dto <- req.as[CreateShopDTO]
      shop <- useCreateShop.execute(dto)
      result = HandlerResult.of(CreateShopResponse(shop))
      res <- Created(result)
    } yield res
  }

  def deleteShop(shopId: Long): F[Response[F]] = {
    useDeleteShop.execute(shopId)
      .flatMap(_ => NoContent())
  }

  def getListShop(): F[Response[F]] = {
    useGetListShop.execute()
      .map(GetListShopResponse)
      .map(HandlerResult.of)
      .flatMap(Ok(_))
  }

}

object ShopHandler {
  def apply[F[_] : Async](shopRepo: ShopRepository[F]): ShopHandler[F] = {
    val useCreateShop = new CreateShop(shopRepo)
    val useDeleteShop = new DeleteShop(shopRepo)
    val useGetListShop = new GetListShop(shopRepo)

    new ShopHandler(useCreateShop, useDeleteShop, useGetListShop)
  }
}
