package dev.ca.handlers

import cats.data.NonEmptyList
import dev.ca.common.exception.ValidationException
import dev.ca.common.validation.ValidationError
import dev.ca.core.author.dto.CreateAuthorDTO
import dev.ca.core.author.entities.Author
import dev.ca.core.book.dto.CreateBookDTO
import dev.ca.core.book.entities.{Book, BookNameEmptyError, BookValidationError}
import dev.ca.core.shop.dto.CreateShopDTO
import dev.ca.core.shop.entities.Shop
import io.circe.{Decoder, Encoder}
import io.circe._
import io.circe.generic.semiauto._
import io.circe.syntax._


// Если очень нужно, можно все кодеки закинуть в отдельный подпакет и раскидать по разным файлам
// Суть в том что никто уровнем ниже контроллеров не должен знать что его данные могут быть закодированны/ раскодированы в/из json
object HandlerCodec
  extends DTOCodec
  with EntityCodec
  with ResponseCodec
  with ResultCodec
  with ExceptionCodec
  with ValidationErrorCodec
  with CommonCodec

trait EntityCodec {
  implicit val bookJsonEncoder: Encoder[Book] = deriveEncoder
  implicit val authorJsonEncoder: Encoder[Author] = deriveEncoder
  implicit val shopJsonEncoder: Encoder[Shop] = deriveEncoder
}

trait DTOCodec {
  implicit val createBookDTOJsonDecoder: Decoder[CreateBookDTO] = deriveDecoder
  implicit val createAuthorDTOJsonDecoder: Decoder[CreateAuthorDTO] = deriveDecoder
  implicit val createShopDTOJsonDecoder: Decoder[CreateShopDTO] = deriveDecoder
}

trait ResponseCodec extends EntityCodec {
  implicit val controllerResponseJsonEncoder: Encoder[HandlerResponse] = Encoder.instance {
    case r: CreateBookResponse => r.asJson
    case r: GetListBookResponse => r.asJson
    case r: CreateAuthorResponse => r.asJson
    case r: GetListAuthorResponse => r.asJson
    case r: CreateShopResponse => r.asJson
    case r: GetListShopResponse => r.asJson
  }

  implicit val createBookResponseJsonEncoder: Encoder[CreateBookResponse] = deriveEncoder
  implicit val getListBookResponseJsonEncoder: Encoder[GetListBookResponse] = deriveEncoder
  implicit val createAuthorResponseJsonEncoder: Encoder[CreateAuthorResponse] = deriveEncoder
  implicit val getListAuthorResponseJsonEncoder: Encoder[GetListAuthorResponse] = deriveEncoder
  implicit val createShopResponseJsonEncoder: Encoder[CreateShopResponse] = deriveEncoder
  implicit val getListShopResponseJsonEncoder: Encoder[GetListShopResponse] = deriveEncoder
}

trait ResultCodec extends ResponseCodec with ExceptionCodec{
  implicit val controllerResultJsonEncoder: Encoder[HandlerResult] = Encoder.instance {
    case r: DataHandlerResult => r.asJson
    case r: ErrorHandlerResult => r.asJson
  }

  implicit val dataControllerResultJsonEncoder: Encoder[DataHandlerResult] = deriveEncoder
  implicit val errorControllerResultJsonEncoder: Encoder[ErrorHandlerResult] = deriveEncoder
}

trait ExceptionCodec extends ValidationErrorCodec {
  implicit val throwableEncoder: Encoder[Throwable] = Encoder.instance {
    case ex: ValidationException => ex.asJson
    case ex => ex.getMessage.asJson
  }

  implicit val validationExceptionJsonEncoder: Encoder[ValidationException] = Encoder.instance(_.errors.asJson)
}

trait ValidationErrorCodec extends CommonCodec{
  implicit def validationErrorJsonEncoder: Encoder[ValidationError] = Encoder.instance { error =>
    def errToJson(e: ValidationError): Json = {
      Json.obj((e.field, e.value match {
        case Left(value) => Json.fromString(value)
        case Right(value) => value.map(errToJson).fold(Json.fromString("{}"))((a, b) => a.deepMerge(b))
      }))
    }

    Json.obj(
      ("field", error.field.asJson),
      ("value", error.value match {
        case Left(value) => value.asJson
        case Right(value) => value.map(errToJson).foldLeft(Json.fromString("{}"))((a, b) => a.deepMerge(b))
      })
    )
  }

  implicit val nelValidationErrorJsonEncoder: Encoder[NonEmptyList[ValidationError]] = Encoder.instance(_.toList.asJson)
}

trait CommonCodec {
  implicit def eitherEncoder[A, B](implicit a: Encoder[A], b: Encoder[B]): Encoder[Either[A, B]] = {
    case Left(value) => a(value)
    case Right(value) => b(value)
  }
}
