package dev.ca.repositories

import cats.effect.Async
import cats.effect.std.AtomicCell
import cats.syntax.all._
import dev.ca.core.shop.dto.CreateShopDTO
import dev.ca.core.shop.entities.Shop
import dev.ca.core.shop.ports.ShopRepository

final class InMemoryShopRepository[F[_]] private (cell: AtomicCell[F, Map[Long, Shop]])(
  implicit F: Async[F]
) extends ShopRepository[F] {

  override def getAll(): F[List[Shop]] = {
    cell.get.map(_.values.toList)
  }

  override def getById(bookId: Long): F[Option[Shop]] = {
    cell.get.map(_.get(bookId))
  }

  override def create(dto: CreateShopDTO): F[Shop] = {
    cell.modify { authors =>
      val maxId = authors.keys.maxOption.getOrElse(1L)
      val authorWithId = Shop(maxId + 1L, dto.name)
      val updated = authors.updated(authorWithId.id, authorWithId)

      (updated, authorWithId)
    }
  }

  override def deleteById(bookId: Long): F[Unit] = {
    cell.update(_.removed(bookId))
  }
}


object InMemoryShopRepository {
  def apply[F[_] : Async](source: Map[Long, Shop]): F[ShopRepository[F]] = {
    AtomicCell[F].of(source).map { cell =>
      new InMemoryShopRepository(cell)
    }
  }
}
