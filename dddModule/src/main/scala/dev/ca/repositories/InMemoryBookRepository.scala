package dev.ca.repositories

import cats.effect.Async
import cats.effect.std.AtomicCell
import cats.implicits.toFunctorOps
import dev.ca.core.book.dto.CreateBookDTO
import dev.ca.core.book.entities.Book
import dev.ca.core.book.ports.BookRepository

final class InMemoryBookRepository[F[_]] private (cell: AtomicCell[F, Map[Long, Book]])(
  implicit F: Async[F]
) extends BookRepository[F] {

  override def getAll(): F[List[Book]] = {
    cell.get.map(_.values.toList)
  }

  override def getById(bookId: Long): F[Option[Book]] = {
    cell.get.map(_.get(bookId))
  }

  override def create(dto: CreateBookDTO): F[Book] = {
    cell.modify { authors =>
      val maxId = authors.keys.maxOption.getOrElse(1L)
      val authorWithId = Book(maxId + 1L, dto.name)
      val updated = authors.updated(authorWithId.id, authorWithId)

      (updated, authorWithId)
    }
  }

  override def deleteById(bookId: Long): F[Unit] = {
    cell.update(_.removed(bookId))
  }
}


object InMemoryBookRepository {
  def apply[F[_] : Async](source: Map[Long, Book]): F[BookRepository[F]] = {
    AtomicCell[F].of(source).map { cell =>
      new InMemoryBookRepository(cell)
    }
  }
}
