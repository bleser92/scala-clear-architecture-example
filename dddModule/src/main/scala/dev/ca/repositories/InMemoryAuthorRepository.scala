package dev.ca.repositories

import cats.effect.Async
import cats.effect.std.AtomicCell
import cats.implicits.toFunctorOps
import dev.ca.core.author.dto.CreateAuthorDTO
import dev.ca.core.author.entities.Author
import dev.ca.core.author.ports.AuthorRepository


final class InMemoryAuthorRepository[F[_]] private (cell: AtomicCell[F, Map[Long, Author]])(
  implicit F: Async[F]
) extends AuthorRepository[F] {

  override def getAll(): F[List[Author]] = {
    cell.get.map(_.values.toList)
  }

  override def getById(authorId: Long): F[Option[Author]] = {
    cell.get.map(_.get(authorId))
  }

  override def create(dto: CreateAuthorDTO): F[Author] = {
    cell.modify { authors =>
      val maxId = authors.keys.maxOption.getOrElse(1L)
      val authorWithId = Author(maxId + 1L, dto.name)
      val updated = authors.updated(authorWithId.id, authorWithId)

      (updated, authorWithId)
    }
  }

  override def deleteById(bookId: Long): F[Unit] = {
    cell.update(_.removed(bookId))
  }
}


object InMemoryAuthorRepository {
  def apply[F[_] : Async](source: Map[Long, Author]): F[AuthorRepository[F]] = {
    AtomicCell[F].of(source).map { cell =>
      new InMemoryAuthorRepository(cell)
    }
  }
}