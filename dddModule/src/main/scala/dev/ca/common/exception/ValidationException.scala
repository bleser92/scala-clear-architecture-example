package dev.ca.common.exception

import cats.data.NonEmptyList
import dev.ca.common.validation.ValidationError

final case class ValidationException(errors: NonEmptyList[ValidationError]) extends Exception {
  override def getMessage: String = {
    errors.toString()
  }
}
