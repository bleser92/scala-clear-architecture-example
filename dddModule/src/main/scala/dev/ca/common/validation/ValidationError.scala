package dev.ca.common.validation

import cats.implicits.catsSyntaxEitherId

trait ValidationError {
  def field: String
  def value: Either[String, List[ValidationError]]

  final protected def message(msg: String): Either[String, List[ValidationError]] =
    msg.asLeft[List[ValidationError]]
}


