package dev.ca.application

import cats.effect.Async
import com.comcast.ip4s.IpLiteralSyntax
import org.http4s.ember.server.EmberServerBuilder
import cats.syntax.all._
import dev.ca.api.{AuthorRouter, BookRouter, ShopRouter}
import dev.ca.handlers.{AuthorHandler, BookHandler, ShopHandler}
import dev.ca.repositories.{InMemoryAuthorRepository, InMemoryBookRepository, InMemoryShopRepository}
import org.http4s.HttpRoutes


class HttpServer[F[_] : Async] {
  private val errorHandler = new HttpErrorHandler()

  def run(): F[Unit] = {
    collectRouters().flatMap { routers =>
      EmberServerBuilder
        .default[F]
        .withHost(ipv4"0.0.0.0")
        .withPort(port"8888")
        .withHttpApp(routers.orNotFound)
        .withErrorHandler(errorHandler.handle)
        .withoutTLS
        .build
        .use(_ => Async[F].never[Unit])
    }
  }

  // Тут нужно подумать как лучше сделать
  private def collectRouters(): F[HttpRoutes[F]] = {
    for {
      bookRepo <- InMemoryBookRepository(Map.empty)
      authorRepo <- InMemoryAuthorRepository(Map.empty)
      shopRepo <- InMemoryShopRepository(Map.empty)

      bookController = BookHandler(bookRepo)
      authorController = AuthorHandler(authorRepo)
      shopController = ShopHandler(shopRepo)
    } yield {
      List(
        new BookRouter(bookController),
        new AuthorRouter(authorController),
        new ShopRouter(shopController)
      ).flatMap(_.routes).reduce(_ <+> _)
    }
  }
}
