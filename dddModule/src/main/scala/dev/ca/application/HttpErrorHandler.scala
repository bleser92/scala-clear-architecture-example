package dev.ca.application

import cats.effect.kernel.Async
import org.http4s.Response
import org.http4s.Status.{BadRequest, InternalServerError}
import org.http4s.circe.CirceEntityCodec._
import dev.ca.handlers.HandlerCodec._
import dev.ca.common.exception.ValidationException
import dev.ca.handlers.HandlerResult
import org.http4s.dsl.Http4sDsl

class HttpErrorHandler[F[_] : Async] extends Http4sDsl[F] {
  type ErrorHandler = PartialFunction[Throwable, F[Response[F]]]

  def handle: ErrorHandler = {
    case ex: ValidationException => BadRequest(HandlerResult.of(ex))
    case ex => InternalServerError(HandlerResult.of(ex))
  }

}
