package dev.ca.application

import cats.effect.{ExitCode, IO, IOApp}

object AppMain extends IOApp {

  override def run(args: List[String]): IO[ExitCode] = {
    val server = new HttpServer[IO]()

    server.run().as(ExitCode.Success)
  }
}
