package dev.ca.api

import cats.effect.Async
import cats.syntax.all._
import dev.ca.handlers.BookHandler
import dev.ca.core.book.dto.CreateBookDTO
import io.circe.generic.semiauto.{deriveCodec, deriveDecoder}
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl


class BookRouter[F[_] : Async](bookController: BookHandler[F]) extends BaseRouter[F] {

  private val publicRoutes = HttpRoutes.of[F] {
    case GET -> Root / "api" / "books" => bookController.getListBook()
    case req@POST -> Root / "api" / "books" => bookController.createBook(req)
    case DELETE -> Root / "api" / "books" / LongVar(bookId) => bookController.deleteBook(bookId)
  }

  def routes: List[HttpRoutes[F]] = List(publicRoutes)
}


