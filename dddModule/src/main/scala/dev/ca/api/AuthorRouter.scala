package dev.ca.api

import cats.effect.Async
import dev.ca.handlers.AuthorHandler
import org.http4s.HttpRoutes

class AuthorRouter[F[_] : Async](authorController: AuthorHandler[F]) extends BaseRouter[F]{

  private val publicRoutes = HttpRoutes.of[F] {
    case GET -> Root / "api" / "authors" => authorController.getListAuthor()
    case req@POST -> Root / "api" / "authors" => authorController.createAuthor(req)
    case  DELETE -> Root / "api" / "authors" / LongVar(authorId) => authorController.deleteAuthor(authorId)
  }

  override def routes: List[HttpRoutes[F]] = List(publicRoutes)
}
