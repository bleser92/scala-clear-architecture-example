package dev.ca.api

import cats.effect.Async
import dev.ca.handlers.ShopHandler
import org.http4s.HttpRoutes

class ShopRouter[F[_] : Async](shopController: ShopHandler[F]) extends BaseRouter[F] {

  private val publicRoutes = HttpRoutes.of[F] {
    case GET -> Root / "api" / "shops" => shopController.getListShop()
    case req@POST -> Root / "api" / "shops" => shopController.createShop(req)
    case DELETE -> Root / "api" / "shops" / LongVar(shopId) => shopController.deleteShop(shopId)
  }



  override def routes: List[HttpRoutes[F]] = List(publicRoutes)
}
