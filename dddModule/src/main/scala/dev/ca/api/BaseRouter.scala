package dev.ca.api

import cats.effect.Async
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl

abstract class BaseRouter[F[_] : Async] extends Http4sDsl[F] {

  def routes: List[HttpRoutes[F]]
}
