package dev.ca.core.author

import cats.data.{NonEmptyList, Validated, ValidatedNel}
import cats.effect.Async
import cats.syntax.all._
import dev.ca.core.author.dto.CreateAuthorDTO
import dev.ca.core.author.entities.{AuthorNameEmptyError, AuthorValidationError}

private[author] class AuthorValidator[F[_] : Async] {
  type ValidationResult[T] = Validated[NonEmptyList[AuthorValidationError], T]

  def validateCreateBook (dto: CreateAuthorDTO): F[ValidationResult[CreateAuthorDTO]] = {
    validateBookName(dto.name)
      .map(_ => dto)
      .pure[F]
  }

  private def validateBookName(name: String): ValidatedNel[AuthorValidationError, String] = {
    name match {
      case _ if name.isEmpty => AuthorNameEmptyError.invalidNel
      case _ => name.validNel
    }
  }
}
