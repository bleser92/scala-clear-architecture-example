package dev.ca.core.author.usecases

import cats.effect.Async
import dev.ca.core.author.ports.AuthorRepository

class DeleteAuthor[F[_] : Async](
  private val authorRepo: AuthorRepository[F]
) {

  def execute(authorId: Long): F[Unit] = {
    authorRepo.deleteById(authorId)
  }

}
