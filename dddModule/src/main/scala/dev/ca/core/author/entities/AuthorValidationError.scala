package dev.ca.core.author.entities

import dev.ca.common.validation.ValidationError

sealed abstract class AuthorValidationError extends ValidationError

case object AuthorNameEmptyError extends AuthorValidationError {
  override val field: String = "name"
  override val value: Either[String, List[ValidationError]] = message("Author name is empty")
}
