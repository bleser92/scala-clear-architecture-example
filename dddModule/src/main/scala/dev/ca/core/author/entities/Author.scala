package dev.ca.core.author.entities

case class Author(id: Long, name: String)
