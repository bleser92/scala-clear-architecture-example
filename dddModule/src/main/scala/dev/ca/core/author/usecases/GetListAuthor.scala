package dev.ca.core.author.usecases

import cats.effect.Async
import dev.ca.core.author.entities.Author
import dev.ca.core.author.ports.AuthorRepository

class GetListAuthor[F[_] : Async](
  private val authorRepo: AuthorRepository[F]
) {

  def execute(): F[List[Author]] = {
    authorRepo.getAll()
  }

}
