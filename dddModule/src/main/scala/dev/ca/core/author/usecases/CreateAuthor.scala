package dev.ca.core.author.usecases

import cats.data.EitherT
import cats.effect.Async
import cats.syntax.all._
import dev.ca.common.exception.ValidationException
import dev.ca.core.author.AuthorValidator
import dev.ca.core.author.dto.CreateAuthorDTO
import dev.ca.core.author.entities.Author
import dev.ca.core.author.ports.AuthorRepository

class CreateAuthor[F[_] : Async](
  private val authorRepo: AuthorRepository[F]
) {

  private val validator = new AuthorValidator()

  def execute(dto: CreateAuthorDTO): F[Author] = {
    EitherT(validator.validateCreateBook(dto).map(_.toEither))
      .valueOrF(nel => Async[F].raiseError[CreateAuthorDTO](ValidationException(nel)))
      .flatMap(authorRepo.create)
  }

}
