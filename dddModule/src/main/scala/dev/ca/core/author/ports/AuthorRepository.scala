package dev.ca.core.author.ports

import dev.ca.core.author.dto.CreateAuthorDTO
import dev.ca.core.author.entities.Author

trait AuthorRepository[F[_]] {

  def create(dto: CreateAuthorDTO): F[Author]
  def getAll(): F[List[Author]]
  def deleteById(authorId: Long): F[Unit]
  def getById(authorId: Long): F[Option[Author]]

}
