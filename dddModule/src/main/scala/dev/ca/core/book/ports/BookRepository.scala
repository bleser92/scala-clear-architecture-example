package dev.ca.core.book.ports

import dev.ca.core.book.dto.CreateBookDTO
import dev.ca.core.book.entities.Book

trait BookRepository[F[_]] {

  def getAll(): F[List[Book]]
  def getById(bookId: Long): F[Option[Book]]
  def create(book: CreateBookDTO): F[Book]
  def deleteById(bookId: Long): F[Unit]

}


