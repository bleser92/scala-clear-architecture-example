package dev.ca.core.book

import cats.data.{NonEmptyList, Validated, ValidatedNel}
import cats.effect.Async
import cats.implicits.{catsSyntaxApplicativeId, catsSyntaxValidatedId}
import dev.ca.core.book.dto.CreateBookDTO
import dev.ca.core.book.entities.{BookNameEmptyError, BookValidationError}

private[book] class BookValidator[F[_] : Async] {
  type ValidationResult[T] = Validated[NonEmptyList[BookValidationError], T]

  def validateCreateBook(dto: CreateBookDTO): F[ValidationResult[CreateBookDTO]] = {
    validateBookName(dto.name)
      .map(_ => dto)
      .pure[F]
  }

  private def validateBookName(name: String): ValidatedNel[BookValidationError, String] = {
    name match {
      case _ if name.isEmpty => BookNameEmptyError.invalidNel
      case _ => name.validNel
    }
  }
}
