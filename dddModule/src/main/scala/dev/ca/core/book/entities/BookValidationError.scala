package dev.ca.core.book.entities

import dev.ca.common.validation.ValidationError

sealed abstract class BookValidationError extends ValidationError

case object BookNameEmptyError extends BookValidationError {
  override val field: String = "name"
  override val value: Either[String, List[ValidationError]] = message("Book name is empty")
}
