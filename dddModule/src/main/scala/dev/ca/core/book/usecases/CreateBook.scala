package dev.ca.core.book.usecases

import cats.data.EitherT
import cats.effect.Async
import cats.syntax.all._
import dev.ca.common.exception.ValidationException
import dev.ca.core.book.BookValidator
import dev.ca.core.book.dto.CreateBookDTO
import dev.ca.core.book.entities.Book
import dev.ca.core.book.ports.BookRepository


class CreateBook[F[_] : Async](private val bookRepository: BookRepository[F]) extends (CreateBookDTO => F[Book]){

  private val validator = new BookValidator()


  override def apply(dto: CreateBookDTO): F[Book] = {
    EitherT(validator.validateCreateBook(dto).map(_.toEither))
      .valueOrF(nel => Async[F].raiseError[CreateBookDTO](ValidationException(nel)))
      .flatMap(bookRepository.create)
  }

}
