package dev.ca.core.book.entities

case class Book(
  id: Long,
  name: String
)
