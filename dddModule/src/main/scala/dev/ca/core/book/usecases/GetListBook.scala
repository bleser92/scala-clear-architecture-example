package dev.ca.core.book.usecases

import cats.effect.Async
import dev.ca.core.book.entities.Book
import dev.ca.core.book.ports.BookRepository

class GetListBook[F[_] : Async](private val bookRepository: BookRepository[F]) {

  def execute(): F[List[Book]] = {
    bookRepository.getAll()
  }
}
