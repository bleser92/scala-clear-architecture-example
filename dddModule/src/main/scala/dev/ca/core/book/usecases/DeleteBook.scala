package dev.ca.core.book.usecases

import cats.effect.Async
import dev.ca.core.book.ports.BookRepository

class DeleteBook[F[_] : Async](private val bookRepo: BookRepository[F]) {

  def execute(bookId: Long): F[Unit] = {
    bookRepo.deleteById(bookId)
  }
}
