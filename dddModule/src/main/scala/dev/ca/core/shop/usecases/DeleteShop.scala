package dev.ca.core.shop.usecases

import cats.effect.Async
import dev.ca.core.shop.ports.ShopRepository

class DeleteShop[F[_] : Async](private val shopRepo: ShopRepository[F]) {

  def execute(shopId: Long): F[Unit] = {
    shopRepo.deleteById(shopId)
  }
}
