package dev.ca.core.shop.ports

import dev.ca.core.shop.dto.CreateShopDTO
import dev.ca.core.shop.entities.Shop

trait ShopRepository[F[_]] {

  def getAll(): F[List[Shop]]
  def getById(shopId: Long): F[Option[Shop]]
  def create(book: CreateShopDTO): F[Shop]
  def deleteById(shopId: Long): F[Unit]

}
