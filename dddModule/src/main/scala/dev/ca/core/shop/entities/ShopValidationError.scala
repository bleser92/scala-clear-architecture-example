package dev.ca.core.shop.entities

import dev.ca.common.validation.ValidationError

sealed abstract class ShopValidationError extends ValidationError

case object ShopNameEmptyError extends ShopValidationError {
  override val field: String = "name"
  override val value: Either[String, List[ValidationError]] = message("Shop name is empty")
}
