package dev.ca.core.shop.usecases

import cats.data.EitherT
import cats.effect.Async
import cats.syntax.all._
import dev.ca.common.exception.ValidationException
import dev.ca.core.shop.ShopValidator
import dev.ca.core.shop.dto.CreateShopDTO
import dev.ca.core.shop.entities.Shop
import dev.ca.core.shop.ports.ShopRepository

class CreateShop[F[_] : Async](private val shopRepo: ShopRepository[F]) {

  private val validator = new ShopValidator()

  def execute(dto: CreateShopDTO): F[Shop] = {
    EitherT(validator.validateCreateBook(dto).map(_.toEither))
      .valueOrF(nel => Async[F].raiseError[CreateShopDTO](ValidationException(nel)))
      .flatMap(shopRepo.create)
  }

}
