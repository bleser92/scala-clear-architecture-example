package dev.ca.core.shop

import cats.data.{NonEmptyList, Validated, ValidatedNel}
import cats.effect.kernel.Async
import cats.syntax.all._
import dev.ca.core.shop.dto.CreateShopDTO
import dev.ca.core.shop.entities.{ShopNameEmptyError, ShopValidationError}

class ShopValidator[F[_] : Async] {
  type ValidationResult[T] = Validated[NonEmptyList[ShopValidationError], T]

  def validateCreateBook(dto: CreateShopDTO): F[ValidationResult[CreateShopDTO]] = {
    validateBookName(dto.name)
      .map(_ => dto)
      .pure[F]
  }

  private def validateBookName(name: String): ValidatedNel[ShopValidationError, String] = {
    name match {
      case _ if name.isEmpty => ShopNameEmptyError.invalidNel
      case _ => name.validNel
    }
  }
}
