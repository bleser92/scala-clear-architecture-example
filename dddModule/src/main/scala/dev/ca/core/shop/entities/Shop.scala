package dev.ca.core.shop.entities

case class Shop(id: Long, name: String)
