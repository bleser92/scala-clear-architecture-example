package dev.ca.core.shop.usecases

import cats.effect.Async
import dev.ca.core.shop.entities.Shop
import dev.ca.core.shop.ports.ShopRepository

class GetListShop[F[_] : Async](private val shopRepo: ShopRepository[F]) {

  def execute(): F[List[Shop]] = {
    shopRepo.getAll()
  }
}
