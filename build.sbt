import Dependencies._



ThisBuild / scalaVersion := "2.13.10"
ThisBuild / scalacOptions ++= scalaCompileOptions



lazy val scalaCompileOptions = Seq(
  "-encoding", "utf-8",
  "-deprecation",
  "-unchecked",
  "-explaintypes",
  "-feature",
  "-language:existentials",
  "-language:higherKinds",
  "-language:implicitConversions",
)



lazy val dddModule = project
  .settings(
    libraryDependencies ++= cats ++ http4s ++ circe ++ logger ++ scalaTest ++ mockito ++ List (
      scalactic,

    )
  )
