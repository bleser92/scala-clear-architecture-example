addSbtPlugin("com.leobenkel" % "umlclassdiagram" % "1.2.0")

addCompilerPlugin("org.typelevel" % "kind-projector" % "0.13.2" cross CrossVersion.full)
