import sbt._

object Dependencies {
  private val http4sVersion = "1.0.0-M38"


  val cats = List("org.typelevel" %% "cats-effect" % "3.4.5")

  val scalactic = "org.scalactic" %% "scalactic" % "3.2.15"

  val scalaTest = List(
    "org.scalatest" %% "scalatest" % "3.2.15" % Test,
    "org.typelevel" %% "cats-effect-testing-scalatest" % "1.4.0" % Test
  )

  val mockito = List (
    "org.mockito" %% "mockito-scala" % "1.17.12" % Test,
    "org.mockito" %% "mockito-scala-scalatest" % "1.17.12" % Test,
    "org.mockito" %% "mockito-scala-cats" % "1.17.12" % Test,
  )

  val http4s = List(
    "org.http4s" %% "http4s-dsl" % http4sVersion,
    "org.http4s" %% "http4s-ember-server" % http4sVersion,
    "org.http4s" %% "http4s-circe" % http4sVersion,
  )

//  val refined = List(
//    "eu.timepit" %% "refined" % "0.10.1",
//    "eu.timepit" %% "refined-cats" % "0.10.1"
//  )

  val circe = List(
    "io.circe" %% "circe-core" % "0.14.3",
    "io.circe" %% "circe-generic" % "0.14.3",
    "io.circe" %% "circe-parser" % "0.14.3"
  )

//  val tapir = List(
//    "com.softwaremill.sttp.tapir" %% "tapir-core" % "1.2.5",
//    "com.softwaremill.sttp.tapir" %% "tapir-openapi-docs" % "1.2.5",
//    "com.softwaremill.sttp.tapir" %% "tapir-json-circe" % "1.2.5",
//    "com.softwaremill.sttp.tapir" %% "tapir-http4s-server" % "1.2.5",
//
//    "org.http4s" %% "http4s-ember-server" % "0.23.17",
//  )

  val logger = List(
    "com.typesafe.scala-logging" %% "scala-logging" % "3.9.5",
    "ch.qos.logback" % "logback-classic" % "1.2.11"
  )

}